﻿// ThreadsVsTasks
// Copyright (C) 2018 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading;

namespace DustInTheWind.ThreadsVsTasks.Generators
{
    internal class MultiThreadTreeGenerator : ITreeGenerator
    {
        private readonly int levelCount;

        private readonly List<Thread> threads = new List<Thread>();

        public MultiThreadTreeGenerator(int levelCount)
        {
            if (levelCount < 0) throw new ArgumentOutOfRangeException(nameof(levelCount));

            this.levelCount = levelCount;
        }

        public Node Generate()
        {
            Node rootNode = GenerateNode(0);

            foreach (Thread thread in threads)
                thread.Join();

            return rootNode;
        }

        private Node GenerateNode(int generation)
        {
            Node node = new Node();

            Thread thread = new Thread(o =>
            {
                node.Values = RandomNumbers.Generate();
            });
            thread.Start(node);
            threads.Add(thread);

            node.LeftNode = generation < levelCount ? GenerateNode(generation + 1) : null;
            node.RightNode = generation < levelCount ? GenerateNode(generation + 1) : null;

            return node;
        }
    }
}